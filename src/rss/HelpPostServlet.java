package rss;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rometools.rome.feed.synd.SyndCategory;
import com.rometools.rome.feed.synd.SyndCategoryImpl;
import com.rometools.rome.feed.synd.SyndContentImpl;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndEntryImpl;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.feed.synd.SyndFeedImpl;
import com.rometools.rome.feed.synd.SyndLink;
import com.rometools.rome.feed.synd.SyndLinkImpl;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedOutput;

import dao.CategoryDAO;
import dao.HelpPostDAO;
import dto.HelpPostDTO;

/**
 * Servlet implementation class HelpPostServlet
 */
@WebServlet("/rss/posts")
public class HelpPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HelpPostServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SyndFeed feed = new SyndFeedImpl();
		feed.setFeedType("rss_2.0");
		feed.setTitle("Help Posts");
		feed.setDescription("Help posts");
		feed.setLink("https://europa.eu/newsroom/calendar.xml_en?field_nr_events_by_topic_tid=151");
		List<SyndEntry> entries = new LinkedList<SyndEntry>();
		for (HelpPostDTO helpPost : new HelpPostDAO().getAllPost()) {
			SyndEntryImpl entry = new SyndEntryImpl();
			entry.setTitle(helpPost.getName());
			SyndContentImpl content = new SyndContentImpl();
			content.setValue(helpPost.getDescription());
			entry.setDescription(content);
			entry.setLink(helpPost.getImageUrl());
			entry.setPublishedDate(Date.from(helpPost.getDatetime().atZone(ZoneId.systemDefault()).toInstant()));
			SyndCategory category = new SyndCategoryImpl();
			String categoryStr = new CategoryDAO().getNameOfCategotyById(helpPost.getId());
			category.setName(categoryStr);
			LinkedList<SyndCategory> categories = new LinkedList<>();
			categories.add(category);
			entry.setCategories(categories);
			entries.add(entry);
		}
		feed.setEntries(entries);
		try {
			response.getWriter().println(new SyndFeedOutput().outputString(feed));
		} catch (FeedException ex) {
			response.sendError(500);
		}

	}

}
