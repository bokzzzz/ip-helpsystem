package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

import connection.ConnectionPool;
import dto.HelpPostDTO;

public class HelpPostDAO {
	public boolean addPost(HelpPostDTO helpPost) {
		String sql = "insert into help values(null, ?, ?, ?, ?, ?, ?, ?, ?)";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setString(1, helpPost.getName());
			statement.setString(2, helpPost.getLocation());
			statement.setString(3, helpPost.getDescription());
			statement.setString(4, helpPost.getImageUrl());
			statement.setString(5, helpPost.getDatetime().toString());
			statement.setInt(6, helpPost.getCategory());
			statement.setInt(7, 0);
			statement.setInt(8, 0);
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;

	}
	public List<HelpPostDTO> getAllPost() {
		List<HelpPostDTO> posts  = new LinkedList<HelpPostDTO>();
		
		String sql = "select * from help where isBlocked=0";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				HelpPostDTO post=new HelpPostDTO();
				post.setId(resultSet.getInt("id"));
				post.setName(resultSet.getString("name"));
				post.setCategory(resultSet.getInt("categoryHelp_id"));
				post.setDescription(resultSet.getString("description"));
				post.setImageUrl(resultSet.getString("imageUrl"));
				post.setLocation(resultSet.getString("location"));
				post.setDatetime(LocalDateTime.parse(resultSet.getString("dateTime"),formatter));
				
				posts.add(post);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return posts;
	}
	public boolean updateBlockPost(Integer id) {
		String sql = "update help set isBlocked = 1 where id= ?";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setInt(1, id);
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
	public boolean updateReportPost(Integer id) {
		String sql = "update help set isReported = 1 where id= ?";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setInt(1, id);
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
}
