package beans;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;


import dao.CategoryDAO;
import dao.HelpPostDAO;
import dto.CategoryDTO;
import dto.HelpPostDTO;

@ManagedBean(name= "helpPost")
@RequestScoped
public class HelpPost implements Serializable {
	private static final long serialVersionUID = 1085410648811649467L;
	private HelpPostDTO helpPostDTO=new HelpPostDTO();
	private List<CategoryDTO> categories=new LinkedList<CategoryDTO>();
	
	 private UIComponent component;

	    public UIComponent getComponent() {
	        return component;
	    }

	    public void setComponent(UIComponent component) {
	        this.component = component;
	    }
	
	public HelpPost() {
		super();
		categories=new CategoryDAO().getAllCategories();
	}
	public String addPost() {
		HelpPostDAO helpPostDAO=new HelpPostDAO();
		FacesContext context = FacesContext.getCurrentInstance();
		if( helpPostDAO.addPost(helpPostDTO)) {
	        context.addMessage(component.getClientId(), new FacesMessage("Post successfully added"));
	        helpPostDTO.setImageUrl("");
	        helpPostDTO.setLocation("");
	        helpPostDTO.setName("");
	        helpPostDTO.setDatetime(null);
	        helpPostDTO.setDescription("");
	        helpPostDTO.setCategory(1);
		}else {
			 context.addMessage(component.getClientId(), new FacesMessage("Error while trying add post"));
		}
		return "";
	}
	public HelpPostDTO getHelpPostDTO() {
		return helpPostDTO;
	}
	public void setHelpPostDTO(HelpPostDTO helpPostDTO) {
		this.helpPostDTO = helpPostDTO;
	}
	public List<CategoryDTO> getCategories() {
		return categories;
	}
	public void setCategories(List<CategoryDTO> categories) {
		this.categories = categories;
	}
}
