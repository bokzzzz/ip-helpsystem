package rest;


import java.net.HttpURLConnection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.HelpPostDAO;

@Path("/")
public class HelpPostREST {
	
	@GET
	@Path("/posts")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllPosts() {
		return Response.ok().entity(new HelpPostDAO().getAllPost()).build();
	}
	
	@GET
	@Path("/post/block/{id}")
	public Response blockPost(@PathParam("id") Integer id) {
		if(new HelpPostDAO().updateBlockPost(id)) {
			return Response.ok().entity("Success blocked!").build();
		}
		return Response.status(HttpURLConnection.HTTP_BAD_REQUEST).build();
	}
	
	@GET
	@Path("/post/report/{id}")
	public Response reportPost(@PathParam("id") Integer id) {
		if(new HelpPostDAO().updateReportPost(id)) {
			return Response.ok().entity("Success reported!").build();
		}
		return Response.status(HttpURLConnection.HTTP_BAD_REQUEST).build();
	}
}
